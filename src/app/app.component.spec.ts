import {TestBed, async} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {AppModule} from "./app.module";
import {BrowserModule, By} from "@angular/platform-browser";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {FormsModule} from "@angular/forms";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {ToastrModule} from "ngx-toastr";
import {NotesComponent} from "./notes/notes.component";
import {NoteComponent} from "./notes/note/note.component";
import {NoteListComponent} from "./notes/note-list/note-list.component";

describe('AppComponent', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                AppComponent,
                NotesComponent,
                NoteComponent,
                NoteListComponent
            ],
            imports: [BrowserModule, HttpClientTestingModule, FormsModule, BrowserAnimationsModule, ToastrModule.forRoot()]
        }).compileComponents();
    }));

    it('should create the app', () => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });

    it(`should have as title 'note-web-app'`, () => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app.title).toEqual('note-web-app');
    });

    it('should render title in a h1 tag', () => {
        const fixture = TestBed.createComponent(AppComponent);
        const compiled = fixture.debugElement.nativeElement;
        expect(compiled.querySelector('h1').textContent).toContain('My Notes - Simple Note Application');
    });

    it('should count words in title', () => {
        const fixture = TestBed.createComponent(AppComponent);
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            const compiled = fixture.debugElement.nativeElement;
            let title = compiled.querySelector('[name=title]');
            title.value = "hello";
            title.dispatchEvent(new Event('input'));
            fixture.detectChanges();
            let wordCount = fixture.debugElement.query(By.css('.word-counter')).nativeElement;
            expect(wordCount.innerText).toContain("5/255");
        });
    });
});
