import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NoteListComponent} from './note-list.component';
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {ToastrModule} from "ngx-toastr";

describe('NoteListComponent', () => {
    let component: NoteListComponent;
    let fixture: ComponentFixture<NoteListComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports:[HttpClientTestingModule, ToastrModule.forRoot()],
            declarations: [NoteListComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(NoteListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
