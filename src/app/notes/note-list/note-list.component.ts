import {Component, OnInit} from '@angular/core';
import {NoteService} from "../../shared/note.service";
import {Note} from "../../shared/note.model";
import {ToastrService} from "ngx-toastr";

@Component({
    selector: 'app-note-list',
    templateUrl: './note-list.component.html',
    styleUrls: ['./note-list.component.css']
})
export class NoteListComponent implements OnInit {

    selectedNote: Note;

    constructor(private service: NoteService, private toast: ToastrService) {
    }

    ngOnInit() {
        this.service.refreshNoteList();
    }

    editNote(note: Note) {
        this.service.formData = Object.assign({}, note);
        this.selectedNote = this.service.formData;
    }

    deleteNote(id: String) {
        if (confirm("Are you sure?")) {
            this.service.deleteNote(id).toPromise().then(res => {
                this.service.refreshNoteList();
                this.service.resetForm();
                this.toast.warning("Note Deleted!", "My Notes");
            });
        }
    }
}
