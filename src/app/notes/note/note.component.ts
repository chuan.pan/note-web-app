import {Component, OnInit} from '@angular/core';
import {NoteService} from "../../shared/note.service";
import {NgForm} from "@angular/forms";
import {ToastrService} from "ngx-toastr";

@Component({
    selector: 'app-note',
    templateUrl: './note.component.html',
    styleUrls: ['./note.component.css']
})
export class NoteComponent implements OnInit {

    constructor(private service: NoteService, private toastr: ToastrService) {
    }

    ngOnInit() {
        this.service.resetForm();
    }

    onSubmit(form: NgForm) {
        if (form.value.id == null || form.value.id === '') {
            this.createNote(form);
        } else {
            this.updateNote(form);
        }
    }

    updateNote(form: NgForm) {
        this.service.updateNote(form.value).subscribe(res => {
            this.toastr.info("Note Updated!", "My Notes");
            this.service.resetForm(form);
            this.service.refreshNoteList();
        });
    }

    createNote(form: NgForm) {
        this.service.saveNote(form.value).subscribe(res => {
            this.toastr.success("Note Saved!", "My Notes");
            this.service.resetForm(form);
            this.service.refreshNoteList();
        });
    }

    resetForm(form: NgForm) {
        this.service.resetForm(form);
    }
}
