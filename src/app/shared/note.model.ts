export class Note {
    id: string;
    title: string;
    content: string;
    updated: string;
    created: string;
}
