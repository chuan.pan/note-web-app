import {TestBed, getTestBed, async} from '@angular/core/testing';
import {ToastrModule} from 'ngx-toastr';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {NoteService} from './note.service';
import {Note} from "./note.model";

describe('NoteService', () => {
    let service: NoteService;
    let httpMock: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule, ToastrModule.forRoot()],
            providers: [NoteService]
        });
        service = getTestBed().get(NoteService);
        httpMock = getTestBed().get(HttpTestingController);
    });

    it('should be created', (done) => {
        const service: NoteService = TestBed.get(NoteService);
        expect(service).toBeTruthy();
        done();
    });

    it('should post to save note', () => {
        let note = {id: "111", title: "Content-1", content: "ABC", updated: "", created: ""};
        service.saveNote(note).subscribe(() => {
            let req = httpMock.expectOne(service.endpoint + '/notes');
            req.flush(note);
            expect(req.request.method).toBe("POST");
            httpMock.verify();
        });
    });

    it("should get all notes from the api", () => {
        let notes: Note[] = [
            {id: "111", title: "Content-1", content: "ABC", updated: "", created: ""},
            {id: "222", title: "Content-2", content: "ABC", updated: "", created: ""}
        ];
        const service: NoteService = TestBed.get(NoteService);
        service.getNotes().subscribe((results: Note[]) => {
            console.log("TEST");
            let req = httpMock.expectOne(service.endpoint + '/notes');
            req.flush(notes);
            console.log(results.length);
            expect(req.request.method).toBe("GET");
            expect(service.list.length).toBe(2);
            httpMock.verify();
        });
    });
});
