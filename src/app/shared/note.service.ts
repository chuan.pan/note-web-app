import {Injectable} from '@angular/core';
import {Note} from "./note.model";
import {HttpClient} from "@angular/common/http";
import {from} from "rxjs";
import {NgForm} from "@angular/forms";
import {ToastrService} from "ngx-toastr";

@Injectable({
    providedIn: 'root'
})
export class NoteService {
    formData: Note;
    list: Note[];
    readonly endpoint = "http://127.0.0.1:8080";

    constructor(private http: HttpClient, private toastr: ToastrService) {
    }

    saveNote(formData: Note) {
        return this.http.post(this.endpoint + '/notes', formData);
    }

    updateNote(formData: Note) {
        return this.http.put(this.endpoint + '/notes/' + formData.id, formData);
    }

    refreshNoteList() {
        this.getNotes()
            .toPromise().then(res => this.list = res as Note[]).catch(err => {
            console.log("Failed to connect to Note services");
            this.toastr.error("Failed to connect to Note service - " + this.endpoint);
        });
    }

    getNotes() {
        return this.http.get(this.endpoint + '/notes');
    }

    deleteNote(id: String) {
        return this.http.delete(this.endpoint + '/notes/' + id);
    }

    resetForm(form?: NgForm) {
        if (from != null && form !== undefined) {
            form.resetForm();
        }
        this.formData = {
            id: "",
            title: "",
            content: "",
            updated: "",
            created: ""
        }
    }
}
